USAGE (for local machine)
-------------------

1. Clone the repo -> git clone git@gitlab.com:gog9292/test_apple.git
2. Go to the root of the project.
3. run docker -> docker-compose up -d
4. Install dependencies for yii2 app. Go into frontend container. Run -> composer install.
5. Run yii2 app. Go into frontend container. Run -> php init.
6. Run migrations. Go into frontend container. Run -> php yii migrate.
7. Create an Admin in DB. Go into frontend container. Run -> php yii admin/create username password.
8. Go to frontend container. Set cron. First run -> crontab -e, add this into file -> */1 * * * * php /app/yii fruit/check-if-rotten
