<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "fruit_tree".
 *
 * @property int $id
 * @property int $type
 * @property int $status
 * @property int $color
 * @property int|null $fall_date
 * @property int|null $eaten_percent
 * @property int $created_at
 * @property int $updated_at
 */
class FruitTree extends ActiveRecord
{
    const FRUITS_TYPES = [
        'APPLE' => 1,
    ];

    const STATUSES = [
        'ON_TREE' => 1,
        'ON_LAND' => 2,
        'ROTTEN' => 3,
    ];

    const COLORS = [
        'RED' => 1,
        'GREEN' => 2,
        'YELLOW' => 3,
    ];

    const FRUIT_COUNT_RANGES = [
        'MIN' => 10,
        'MAX' => 15,
    ];

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fruit_tree';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'status', 'color'], 'required'],
            [['type', 'status', 'color', 'fall_date', 'eaten_percent', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'status' => 'Status',
            'color' => 'Color',
            'fall_date' => 'Fall Date',
            'eaten_percent' => 'Eaten Percent',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @param int $key
     * @return string
     */
    public static function statusesValues(int $key): string
    {
        $as_texts = [
            self::STATUSES['ON_TREE'] => 'Висит на дереве',
            self::STATUSES['ON_LAND'] => 'Лежит на земле',
            self::STATUSES['ROTTEN'] => 'Гнилое яблоко',
        ];

        return $as_texts[$key];
    }

    /**
     * @param int $key
     * @return string
     */
    public static function colorsValues(int $key): string
    {
        $as_texts = [
            self::COLORS['RED'] => 'Красный',
            self::COLORS['GREEN'] => 'Зеленый',
            self::COLORS['YELLOW'] => 'Желтый',
        ];

        return $as_texts[$key];
    }
}
