<?php

declare(strict_types=1);

namespace console\controllers;

use yii\console\Controller;
use common\models\FruitTree;
use yii\console\ExitCode;
use yii\helpers\Console;

class FruitController extends Controller
{
    public function actionCheckIfRotten(): int
    {
        $fruit = FruitTree::find()
            ->where(['status' => FruitTree::STATUSES['ON_LAND']])
            ->andWhere(['<', 'fall_date', time() - (5 * 60 * 60)])
            ->all();

        if (empty($fruit)) {
            $this->stdout("Fruit are not rotten yet.\n", Console::BOLD);
        }

        foreach ($fruit as $item) {
            $item->status = FruitTree::STATUSES['ROTTEN'];
            if (!$item->save()) {
                $this->stderr("Could not save data in DB!\n", Console::FG_RED);
                $this->stderr("{$item->getErrors()}\n", Console::FG_RED);
                return ExitCode::UNAVAILABLE;
            }

            $this->stdout("Fruit is rotten. ID: {$item->id}\n", Console::BOLD);
        }

        return ExitCode::OK;
    }
}
