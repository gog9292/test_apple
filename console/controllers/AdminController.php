<?php

declare(strict_types=1);

namespace console\controllers;

use yii\console\Controller;
use yii\console\ExitCode;
USE yii\helpers\Console;
use common\models\User;

/**
 * Class AdminController
 * @package console\controllers
 */
class AdminController extends Controller
{
    /**
     * @param string $username
     * @param string $password
     * @param string $email
     * @return int
     */
    public function actionCreate(string $username, string $password, string $email): int
    {
        $isUserExist = User::find()
            ->andWhere(['or', ['username' => $username], ['email' => $email]])
            ->exists();

        if (true === $isUserExist) {
            $this->stderr("Admin with username '{$username}' or with email '{$email} already exists!'\n", Console::FG_RED);
            return ExitCode::OK;
        }

        $user = new User();
        $user->status = User::STATUS_ACTIVE;
        $user->username = $username;
        $user->email = $email;
        $user->setPassword($password);
        $user->generateAuthKey();

        if (!$user->save()) {
            $this->stderr("Could not save data in DB!\n", Console::FG_RED);
            $this->stderr("{$user->getErrors()}\n", Console::FG_RED);
            return ExitCode::UNAVAILABLE;
        }

        $this->stdout("A new Admin is created\n", Console::BOLD);
        $this->stdout("Username: {$username}\n", Console::BOLD);
        return ExitCode::OK;
    }
}
