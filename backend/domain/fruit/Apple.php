<?php

declare(strict_types=1);

namespace backend\domain\fruit;

use common\models\FruitTree;
use yii\db\Exception;

/**
 * Class Apple
 * @package backend\domain\fruit
 */
class Apple
{
    /**
     * @return string
     */
    public function name(): string
    {
        return 'Apple';
    }

    /**
     * @throws Exception
     */
    public function generateRandomTree()
    {
        $this->deleteTree();

        $newApplesCountOnTree = rand(
            FruitTree::FRUIT_COUNT_RANGES['MIN'],
            FruitTree::FRUIT_COUNT_RANGES['MAX']
        );

        $rand_key = array_rand(FruitTree::COLORS);

        for ($i = 0; $i < $newApplesCountOnTree; $i++) {
            $newApple = new FruitTree();
            $newApple->type = FruitTree::FRUITS_TYPES['APPLE'];
            $newApple->status = FruitTree::STATUSES['ON_TREE'];
            $newApple->color = FruitTree::COLORS[$rand_key];
            $newApple->eaten_percent = 0;
            if (!$newApple->save()) {
                throw new Exception(print_r($newApple->getErrors(), true));
            }
        }
    }

    /**
     *
     */
    public function deleteTree()
    {
        FruitTree::deleteAll(['type' => FruitTree::FRUITS_TYPES['APPLE']]);
    }

    /**
     * @return array
     */
    public function getTree(): array
    {
         return FruitTree::find()
            ->where(['type' => FruitTree::FRUITS_TYPES['APPLE']])
            ->all();
    }

    /**
     * @param $fruit
     * @return bool
     */
    public function canEat($fruit): bool
    {
        if (
            $fruit->status === FruitTree::STATUSES['ON_TREE']
            || $fruit->status === FruitTree::STATUSES['ROTTEN']
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param FruitTree $fruit
     * @return bool
     */
    public function onTree(FruitTree $fruit): bool
    {
        if ($fruit->status === FruitTree::STATUSES['ON_TREE']) {
            return true;
        }

        return false;
    }

    /**
     * @param string $id
     * @return bool
     * @throws Exception
     */
    public function fallToGround(string $id): bool
    {
        $fruit = FruitTree::find()
            ->where(['status' => FruitTree::STATUSES['ON_TREE']])
            ->andWhere(['id' => $id])
            ->one();

        if (null === $fruit) {
            throw new Exception('Can not find fruit on the tree with id: ' . $id);
        }

        $fruit->status = FruitTree::STATUSES['ON_LAND'];
        $fruit->fall_date = time();
        if (!$fruit->save()) {
            throw new Exception('Error while updating status to ON_LAND!');
        }

        return true;
    }

    /**
     * @param int $id
     * @param int $eaten_percent
     * @throws Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function eat(int $id, int $eaten_percent)
    {
        $eaten_percent = round(abs($eaten_percent));
        $fruit = FruitTree::find()
            ->where(['id' => $id])
            ->andWhere(['type' => FruitTree::FRUITS_TYPES['APPLE']])
            ->one();

        if (null === $fruit) {
            throw new Exception('Can not find fruit with id: ' . $id);
        }

        if (!$this->canEat($fruit)) {
            throw new Exception('I can not eat the fruit with id: ' . $id);
        }

        if (100 - $fruit->eaten_percent < $eaten_percent) {
            throw new Exception('I can not eat more than I have. Id: ' . $id);
        }

        $fruit->eaten_percent += $eaten_percent;

        if ($fruit->eaten_percent == 100) {
            $fruit->delete();
        } else {
            $fruit->save();
        }
    }
}
