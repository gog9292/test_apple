<?php

/**
 * Here we used simple factory in stead of Factory Method and Abstract factory.
*/
declare(strict_types=1);

namespace backend\domain\fruit;

use common\models\FruitTree;
use yii\base\InvalidArgumentException;
use ReflectionClass;
use ReflectionException;

/**
 * Class Fruit
 * @package backend\domain\fruit
 */
class Fruit
{
    /**
     * @var string
     */
    private $fruit_class;

    /**
     * Fruit constructor.
     * @param string $type
     * @throws ReflectionException
     */
    public function __construct(string $type)
    {
        $type = strtolower($type);


        $availableFruitList = array_change_key_case(FruitTree::FRUITS_TYPES);
        if (array_key_exists($type, $availableFruitList)) {

            $classNameFullInfo = new ReflectionClass(get_class($this));
            $namespace = $classNameFullInfo->getNamespaceName();

            $class = sprintf("\%s\%s", $namespace, ucfirst($type));

            $this->fruit_class = $class;
        } else {
            throw new InvalidArgumentException("{$type} as a fruit type does not exist!");
        }
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return new $this->fruit_class();
    }
}
