<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use backend\domain\fruit\Fruit;
use yii\web\Response;

/**
 * Class FruitTreeController
 * @package backend\controllers
 */
class FruitTreeController extends AppController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'generate-tree', 'fall-to-ground', 'eat'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param string $type
     * @return string
     */
    public function actionIndex(string $type): string
    {
        $currentFruit = $this->getFruit($type);

        $tree = $currentFruit->getTree();

        return $this->render('index', [
            'type' => $type,
            'currentFruit' => $currentFruit,
            'tree' => $tree
        ]);
    }

    /**
     * @param string $type
     * @return Response
     */
    public function actionGenerateTree(string $type): Response
    {
        $fruit = $this->getFruit($type);

        $fruit->generateRandomTree();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param string $type
     * @param int $id
     * @return Response
     */
    public function actionFallToGround(string $type, int $id): Response
    {
        $fruit = $this->getFruit($type);

        $fruit->fallToGround($id);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param string $type
     * @param int $id
     * @param int $eaten_percent
     * @return Response
     */
    public function actionEat(string $type, int $id, int $eaten_percent): Response
    {
        $fruit = $this->getFruit($type);
        $fruit->eat($id, $eaten_percent);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param string $type
     * @return mixed
     */
    private function getFruit(string $type)
    {
        try {
            $fruit = (new Fruit($type))->get();
        } catch (\ReflectionException $e) {
            die($e->getMessage());
        }

        return $fruit;
    }
}
