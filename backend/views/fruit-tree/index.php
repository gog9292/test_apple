<?php

/**
 * @var $this yii\web\View
 * @var $tree common\models\FruitTree;
 * @var $currentFruit
 * @var $type
 */

use yii\Helpers\Url;
use common\models\FruitTree;

$this->title = "Generate {$type} tree";
?>

<div class="site-index">

    <div class="jumbotron">
        <h3>Generate tree with random count of <?= $type ?>s!</h3>

        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['fruit-tree/generate-tree?type=apple']) ?>">Generate <?= $type ?> tree!</a></p>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th>Статус</th>
            <th>Цвет</th>
            <th>Дата появления</th>
            <th>Дата падения</th>
            <th>Съеденный процент</th>
            <th>Сорвать</th>
            <th>Съесть</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($tree as $fruit) { ?>
            <tr>
                <td><?= FruitTree::statusesValues($fruit->status) ?></td>
                <td><?= FruitTree::colorsValues($fruit->color) ?></td>
                <td><?= gmdate("Y-m-d H:i:s", $fruit->created_at); ?></td>
                <td><?= ($fruit->fall_date === 0) ? '--:--:--' : gmdate("Y-m-d H:i:s", $fruit->fall_date); ?></td>
                <td><?= sprintf("%s%%", $fruit->eaten_percent); ?></td>

                <?php if ($currentFruit->onTree($fruit)) { ?>
                    <td><a class="btn btn-md btn-success" href="<?= Url::to(["fruit-tree/fall-to-ground?type={$type}&id={$fruit->id}"]) ?>">Сорвать</a></td>
                <?php } else { ?>
                    <td><a disabled class="btn btn-md btn-success" href="<?= Url::to(["fruit-tree/fall-to-ground?type={$type}&id={$fruit->id}"]) ?>">Сорвать</a></td>
                <?php } ?>

                <td>
                    <?php if ($currentFruit->canEat($fruit)) { ?>
                        <div class="col-xs-4">
                            <input type="number" id="points" name="points" step="1" class="form-control">
                        </div>
                        <a onclick="myFunction(this)" class="btn btn-md btn-success" href="<?= Url::to(["fruit-tree/eat?type={$type}&id={$fruit->id}"]) ?>">Съесть</a>
                    <?php } else { ?>
                        <div class="col-xs-4">
                            <input disabled type="number" id="points" name="points" step="1" class="form-control">
                        </div>
                        <a disabled onclick="myFunction(this)" class="btn btn-md btn-success" href="<?= Url::to(["fruit-tree/eat?type={$type}&id={$fruit->id}"]) ?>">Съесть</a>
                    <?php } ?>
                </td>

            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    function myFunction(el) {
        let x = el.parentNode;
        let percent = x.children[0].children[0].value;
        el.setAttribute('href', el.getAttribute('href') + '&eaten_percent=' + percent);
    }
</script>
