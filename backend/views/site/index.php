<?php

/* @var $this yii\web\View */

use yii\Helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Create Fruit tree!</h1>

        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['fruit-tree/index?type=apple']) ?>">Creat Apple tree!</a></p>
    </div>
</div>
